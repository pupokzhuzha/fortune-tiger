package com.animal.fortune.beauty.tigr.app.description

data class Animal(
    val picture : Int,
    val text : String,
    val name : String
)
