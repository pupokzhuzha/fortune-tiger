package com.animal.fortune.beauty.tigr.app.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.animal.fortune.beauty.tigr.app.R

class Main : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}