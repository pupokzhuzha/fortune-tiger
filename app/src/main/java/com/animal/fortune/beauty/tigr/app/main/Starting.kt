package com.animal.fortune.beauty.tigr.app.main

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.animal.fortune.beauty.tigr.app.R
import com.animal.fortune.beauty.tigr.app.description.replaceActivity

@SuppressLint("CustomSplashScreen")
class Starting : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starting)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(Main())
        }, 500)
    }
}